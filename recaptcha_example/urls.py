from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^thanks/?$', TemplateView.as_view(template_name="thanks.html"), name='thanks'),
    url(r'^$', 'recaptcha_example.views.register', name='register'),

    url(r'^admin/', include(admin.site.urls)),
)
