from django.db import models

class Entry(models.Model):
    name = models.CharField(max_length=100)
    business = models.CharField(max_length=200)
    email = models.EmailField()

from django.contrib import admin
admin.site.register(Entry)

