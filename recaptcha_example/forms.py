from django import forms
from captcha.fields import ReCaptchaField

class RegForm(forms.Form):
    name = forms.CharField(max_length=100)
    business = forms.CharField()
    email = forms.EmailField()

class RecaptchaForm(RegForm):
    captcha = ReCaptchaField()
