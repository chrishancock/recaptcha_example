from django.shortcuts import render
from django.http import HttpResponseRedirect
from forms import RegForm, RecaptchaForm
from myratelimit.decorators import ratelimit
from models import Entry


@ratelimit()
def register(request):
    if getattr(request, 'limited', False):
        reg_form = RecaptchaForm
    else:
        reg_form = RegForm

    if request.method == 'POST':
        form = reg_form(request.POST)
        if form.is_valid():
            e = Entry(
                name=form.cleaned_data['name'],
                business=form.cleaned_data['business'],
                email=form.cleaned_data['email'],
            )
            e.save()
            return HttpResponseRedirect('/thanks')
    else:
        form = reg_form()

    return render(
        request, 'registration.html',
        {
            'form': form,
        },
    )
